package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;

public class RepetitionControl {

    //Loops
        //are control structures that allow code blocks to be executed multiple times

    public static void main(String[] args){

        //While Loop
            //allows us for repetitive use of code, similar to for loops, but are usually for situations where the content to iterate through is indefinite

        /*
        int x = 0;//initial value

        while(x<10){//condition
            System.out.println("(While Loop) Loop Number: " + x);
            x++;//increment/decrement
        }

         */

        //Do-while
            //similar to while loops
            //however, do-while loops will always execute at least once - when loops may not execute at all

        /*
        int y = 10;
        do{
            System.out.println("(This will run at least once) Countdown: " + y);
            y--;
        }while(y>10);

        */

        //For Loop
        //Syntax
        /*
            for(initialValue;condition;iteration){
                //code block
            }
         */

        //Mini-Activity
        //initial value of 0
        //i less than 10
        //increase every loop
        //print "(For Loop) Current Count: " + i);

        /*
        for(int i=0;i<10;i++){
            System.out.println("(For Loop) Current Count: " + i);
        }

         */

        //For Loop with Arrays
        int[] intArray = {100,200,300,400,500};
        for(int i=0; i<intArray.length; i++){
            System.out.println(intArray[i]);
        }

        //For-each Loop with Array
        /*
        Syntax:
            for(dataType itemName: arrayName){
                //code block
            }
         */

        // user cases if you don't know the exact number of elements in the array
        String[] boyBandArray = {"John","Paul","George","Ringo"};
        for(String member: boyBandArray){
            System.out.println(member);
        }

        //Multidimensional Array
        String[][] classroom = new String[3][3];
        //[row][column]

        //First row
        classroom[0][0] = "Jenny";
        classroom[0][1] = "Liza";
        classroom[0][2] = "Rose";
        //Second row
        classroom[1][0] = "Ash";
        classroom[1][1] = "Misty";
        classroom[1][2] = "Brock";
        //Third row
        classroom[2][0] = "Amy";
        classroom[2][1] = "Lulu";
        classroom[2][2] = "Morgan";

        for(int row=0;row<3;row++){
            for(int col=0;col<3;col++){
                System.out.println("classroom [" + row + "][" + col + "] = " + classroom[row][col]);
            }
        }

        //for each loop with multidimensional array
        for(String[] row: classroom){
            for(String col: row){
                System.out.println(col);
            }
        }

        //Print as a matrix
        for(int row = 0; row < classroom.length; row++){
            for(int col = 0; col < classroom[row].length;col++){
                System.out.print(classroom[row][col] + " ");
            }
            System.out.println();
        }

        //Mini Activity
        //create an array and print as matrix
        //***
        //285
        //***

        String[][] matrix = {{"*","*","*"},{"2","8","5"},{"*","*","*"}};

        for(int row = 0; row < matrix.length; row++){
            for(int col = 0; col < matrix[row].length;col++){
                System.out.print(matrix[row][col] + " ");
            }
            System.out.println();
        }

        //for-each with ArrayList
        //Syntax
        /*
        arrayListName.foreach(Consumer<E> -> //code block;
        "->" this is called the lambda operator - is used to separate parameter and implementation
         */

        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(5);
        numbers.add(10);
        numbers.add(15);
        numbers.add(20);
        numbers.add(25);
        System.out.println("ArrayList: " + numbers);

        numbers.forEach(num -> System.out.println("ArrayList: " + num));

        //foreach with hashmaps
        //Syntax
        /*
        hashMapName.foreach((key,value) -> //code block;
         */

        HashMap<String,Integer> grades = new HashMap<String,Integer>(){
            {
                this.put("English", 90);
                this.put("Math", 95);
                this.put("Science", 97);
                this.put("History", 94);
            }
        };

        grades.forEach((subject,grade) -> {
            System.out.println("Hashmaps: " + subject + " : " + grade + "\n");
        });


    }

}
