import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.print("Input an integer whose factorial will be computed. \n");
        System.out.println("Please enter a valid number: ");
        Scanner in = new Scanner(System.in);
        int factorial = 1;
        int num = in.nextInt();

        try{
            int x = 1;
            if(num>0){

                while(x<=num){
                    factorial = x*factorial;
                    if(x==num){
                        System.out.println("The factorial of " + num + " is " + factorial);
                    }
                    x++;
                }
            }

            else if(num==0){
                System.out.println("The factorial of 0 is 1.");
            }
            else {
                System.out.println("Negative input is invalid!");
                System.out.println("Please enter a new valid number: ");
                Scanner nonNega = new Scanner(System.in);
                int newNum = nonNega.nextInt();
                int newFact = 1;

                for(int i=1;i<=newNum;i++){
                    newFact = i*newFact;
                    if(i==newNum){
                        System.out.println("The factorial of " + newNum + " is " + newFact);
                    }
                }

            }

        }
        catch(Exception e){
            System.out.println("Invalid Input!");
            e.printStackTrace();
        }

    }
}